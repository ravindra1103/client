import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withRouter } from 'react-router-dom';

const styles = {
    rightnav: {
        float: 'right'
    },
    logo: {
        width: '125px',
        marginTop: '10px',
        marginBottom: '10px'
    },
    appBar: {
        backgroundColor: '#0a64a0',
        minHeight: 'unset',
        height: 56,
        paddingLeft: 5,
        paddingRight: 5
    },
    flex: {
        flexGrow: 1,
    },
    whiteColor: {
        color: 'white'
    }
};

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
        }
        this.handleNavigate = this.handleNavigate.bind(this);
        this.handleShopRoute = this.handleShopRoute.bind(this);
    }
    handleChange = (event, checked) => {
        this.setState({ auth: checked });
    };

    handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    handleNavigate(event) {
        event.preventDefault();
        this.props.history.push('/');
    }

    handleShopRoute(event) {
        event.preventDefault();
        this.props.history.push('/shop');
    }

    render() {
        const { classes } = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);
        const userName = 'ravindra1103@gmail.com'
        return (
            <AppBar className={classes.appBar} >
                <Toolbar className={classes.appBar}>
                    <a onClick={this.handleNavigate}>ARMOS</a>
                    <div className={classes.flex} ></div>
                    <div>
                        <Tooltip title={userName}>
                            <IconButton className={classes.rightnav}
                                aria-owns={open ? 'menu-appbar' : null}
                                aria-haspopup="true"
                                onClick={this.handleMenu}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                        </Tooltip>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={open}
                            onClose={this.handleClose}
                        >
                            <MenuItem onClick={this.handleClose}>My Profile</MenuItem>
                            <MenuItem onClick={this.handleClose}>Logout</MenuItem>
                        </Menu>
                    </div>

                </Toolbar>
            </AppBar>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Header));