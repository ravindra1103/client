import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Tooltip from '@material-ui/core/Tooltip';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DetailsIcon from '@material-ui/icons/Details';
import LogsIcon from '@material-ui/icons/GridOn';
import InventoryControlsIcon from '@material-ui/icons/Info';
import InventoryIcon from '@material-ui/icons/AddShoppingCart';
import MaintenanceIcon from '@material-ui/icons/Build';

import Home from './containers/Home';
import Login from './components/Login';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',

  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: '#0a64a0',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
  flex: {
    flexGrow: 1,
  },
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      authenticated: false,
      anchorEl: null
    };
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleLogin() {
    this.setState({
      authenticated: true
    });
  }
  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    const { classes, theme } = this.props;
    const { authenticated } = this.state;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const userName = 'support@armos.com';

    return (
      <div className={classes.root}>
        {
          !authenticated && <Login onSubmit={this.handleLogin}/>
        }
        { 
          authenticated && <Fragment>
              <AppBar
                  position="absolute"
                  className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
                >
                  <Toolbar disableGutters={!this.state.open}>
                    <IconButton
                      color="inherit"
                      aria-label="Open drawer"
                      onClick={this.handleDrawerOpen}
                      className={classNames(classes.menuButton, this.state.open && classes.hide)}
                    >
                      <MenuIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" noWrap>
                      ARMOS
                    </Typography>
                    <div className={classes.flex} ></div>
                    <div>
                        <Tooltip title={userName}>
                            <IconButton className={classes.rightnav}
                                aria-owns={open ? 'menu-appbar' : null}
                                aria-haspopup="true"
                                onClick={this.handleMenu}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                        </Tooltip>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={open}
                            onClose={this.handleClose}
                        >
                            <MenuItem onClick={this.handleClose}>My Profile</MenuItem>
                            <MenuItem onClick={this.handleClose}>Logout</MenuItem>
                        </Menu>
                    </div>
                  </Toolbar>
                </AppBar>
                <Drawer
                  variant="permanent"
                  classes={{
                    paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
                  }}
                  open={this.state.open}
                >
                  <div className={classes.toolbar}>
                    <IconButton onClick={this.handleDrawerClose}>
                      {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                  </div>
                  <Divider />
                  <List>
                    <div>
                        <ListItem style={{ backgroundColor: '#F5F5F5' }} button>
                          <ListItemIcon>
                            <DashboardIcon />
                          </ListItemIcon>
                          <ListItemText primary="Dashboard" />
                        </ListItem>
                        <ListItem button>
                          <ListItemIcon>
                            <LogsIcon />
                          </ListItemIcon>
                          <ListItemText primary="Logs" />
                        </ListItem>
                        <ListItem style={{ marginLeft: 10}} button>
                          <ListItemIcon >
                            <MaintenanceIcon style={{ fontSize: 16 }}/>
                          </ListItemIcon>
                          <ListItemText primary="Maintenance" style={{ fontSize : 12 }}/>
                        </ListItem>
                        <ListItem style={{ marginLeft: 10}} button>
                          <ListItemIcon>
                            <InventoryIcon style={{ fontSize: 16 }}/>
                          </ListItemIcon>
                          <ListItemText primary="Inventory" />
                        </ListItem>                                            
                        <ListItem button>
                          <ListItemIcon>
                            <InventoryControlsIcon />
                          </ListItemIcon>
                          <ListItemText primary="Inventory Controls" />
                        </ListItem>
                        <ListItem button>
                          <ListItemIcon>
                            <DetailsIcon />
                          </ListItemIcon>
                          <ListItemText primary="Rig Profile" />
                        </ListItem>
                      </div>                    
                  </List>
                </Drawer>
                <BrowserRouter>
                  <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Switch>
                      <Route exact path="/" component={Home} />
                      <Route exact path="/dashboard" component={Home} />
                    </Switch>
                  </main>
                </BrowserRouter>
          </Fragment>
       
        }
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(App);